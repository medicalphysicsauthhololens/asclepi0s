﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScriptableObjectsHandler : MonoBehaviour {
    
    //Initial Scriptable Components
    public Node node;
    public Link link;

    //Initial Audio Components
    public AudioClip audioClipDoorBell;
    public AudioClip audioCorrectAnswer;
    public AudioClip audioWrongAnswer;
    public AudioClip audioComplete;
    public AudioClip audioFail;
    public AudioSource audioSourceDoor;
    public AudioSource audioSourceXrayView;
    public TextToSpeechManager ttsDoorbell;
    public TextToSpeechManager ttsXrayView;

    //Initial GameObjects
    #region GameObjects
    public GameObject gameObjectStethoscopePoint;
    public GameObject gameObjectDoorbell;
    public GameObject gameObjectPatient;
    public GameObject gameObjectXRayBorderView;
    public GameObject gameObjectXRayView;
    public GameObject gameObjectElectroencephalographicMachine;
    public GameObject gameObjectXRay1;
    public GameObject gameObjectXRay2;
    public GameObject gameObjectXRay3;
    public GameObject gameObjectXRay4;
    public GameObject gameObjectXRay5;
    public GameObject gameObjectAnswerMessage1;
    public GameObject gameObjectAnswerMessage2;
    public GameObject gameObjectAnswerMessage3;
    public GameObject gameObjectAnswerMessage4;
    public GameObject gameObjectAnswerMessage5;
    public GameObject gameObjectBanner;
    public GameObject gameObjectCaseTitleQuestion;
    public GameObject gameObjectCaseDescriptionQuestion;
    public GameObject gameObjectDoorbell_Arrow;
    public GameObject gameObjectECG_Arrow;
    public GameObject gameObjectXRay_Arrow;
    public GameObject gameObjectStethoscope_Arrow;
    public GameObject gameObjectPatient_Arrow;
    #endregion

    //Textures
    public Texture textureECG1;
    public Texture textureECG2;
    public Texture textureXRay;

    //Initial Base Instance
    public static ScriptableObjectsHandler Instance { get; private set; }


    void Start()
    {
        
    }

    void Awake()
    {
        Instance = this;
    }

    public void InspectLink(int selection)
    {
        
        //Custom for Node 5943
        if (node.NodeUID == 5943)
        {
            //Deactivate intro message
            gameObjectBanner.SetActive(false);
            
            //Active doorBell arrow
            gameObjectDoorbell_Arrow.SetActive(true);

            //Create "Correct answer" soundeffect
            audioSourceXrayView.PlayOneShot(audioCorrectAnswer);

            //Try and get a TTS Manager
            TextToSpeechManager ttsDoorbell = null;
            if (gameObjectDoorbell != null)
            {
                ttsDoorbell = gameObjectDoorbell.GetComponent<TextToSpeechManager>();
            }
            ttsDoorbell.SpeakText(node.NodeTitle + "." + node.NodeText);

            //Display message to Xray view
            //Title
            gameObjectCaseTitleQuestion.GetComponent<TextMesh>().text = node.NodeTitle;

            //Description
            gameObjectCaseDescriptionQuestion.GetComponent<TextMesh>().text = Converters.SpliceText(node.NodeText, Constants.SliceText_NodeLineLength);

            //Diplay button 'next'
            gameObjectAnswerMessage1.GetComponent<TextMesh>().text = node.NodeLinks[0].LinkText;

            //Deactivate Tile 1
            gameObjectXRay1.SetActive(false);

            //Change node
            node = node.NodeLinks[0].TargetNode;
        }
        else
        {
            //Inspect if the user select the correct tile
            if (node.NodeLinks[selection - 1].TargetNode == null)
            {
                //Deactivate selected tile
                if (selection == 1) gameObjectXRay1.SetActive(false);
                else if (selection == 2) gameObjectXRay2.SetActive(false);
                else if (selection == 3) gameObjectXRay3.SetActive(false);
                else if (selection == 4) gameObjectXRay4.SetActive(false);
                else if (selection == 5) gameObjectXRay5.SetActive(false);

                //Create "Wrong answer" soundeffect
                audioSourceXrayView.PlayOneShot(audioWrongAnswer);
                
                //Speech hint
                //Try and get a TTS Manager
                TextToSpeechManager ttsXrayView = null;
                if (gameObjectXRayBorderView != null)
                {
                    ttsXrayView = gameObjectXRayBorderView.GetComponent<TextToSpeechManager>();
                }
                ttsXrayView.SpeakText(node.NodeLinks[selection - 1].LinkResult);

                return;
            }
            
            //Change node
            //if(node.NodeUID != 5946  
            node = node.NodeLinks[selection - 1].TargetNode;

            //Create "Correct answer" soundeffect
            audioSourceXrayView.PlayOneShot(audioCorrectAnswer);

            //Deactivate all xray tiles
            gameObjectXRay1.SetActive(false);
            gameObjectXRay2.SetActive(false);
            gameObjectXRay3.SetActive(false);
            gameObjectXRay4.SetActive(false);
            gameObjectXRay5.SetActive(false);

            //Activate tiles
            int tile_counter = 1;
            foreach (Link linkItem in node.NodeLinks)
            {
                if (tile_counter == 1)
                {
                    gameObjectXRay1.SetActive(true);
                    gameObjectAnswerMessage1.GetComponent<TextMesh>().text = Converters.SpliceText(linkItem.LinkText, Constants.SliceText_LinkLineLength);
                }
                else if (tile_counter == 2)
                {
                    gameObjectXRay2.SetActive(true);
                    gameObjectAnswerMessage2.GetComponent<TextMesh>().text = Converters.SpliceText(linkItem.LinkText, Constants.SliceText_LinkLineLength);
                }
                else if (tile_counter == 3)
                {
                    gameObjectXRay3.SetActive(true);
                    gameObjectAnswerMessage3.GetComponent<TextMesh>().text = Converters.SpliceText(linkItem.LinkText, Constants.SliceText_LinkLineLength);
                }
                else if (tile_counter == 4)
                {
                    gameObjectXRay4.SetActive(true);
                    gameObjectAnswerMessage4.GetComponent<TextMesh>().text = Converters.SpliceText(linkItem.LinkText, Constants.SliceText_LinkLineLength);
                }
                else if (tile_counter == 5)
                {
                    gameObjectXRay4.SetActive(true);
                    gameObjectAnswerMessage4.GetComponent<TextMesh>().text = Converters.SpliceText(linkItem.LinkText, Constants.SliceText_LinkLineLength);
                }
                tile_counter++;
            }
            //Display message to Xray view
            //Title
            gameObjectCaseTitleQuestion.GetComponent<TextMesh>().text = node.NodeTitle;

            //Description
            gameObjectCaseDescriptionQuestion.GetComponent<TextMesh>().text = Converters.SpliceText(node.NodeText, Constants.SliceText_NodeLineLength);

        }

        //Active - Deactive Extras
        GenerateMedia();


        //End
        if (node.NodeUID == 6003)
        {
            //Create "Complete" soundeffect
            audioSourceXrayView.PlayOneShot(audioComplete);
            return;
        }
        else if (node.NodeUID == 6004)
        {
            //Create "Fail" soundeffect
            audioSourceXrayView.PlayOneShot(audioFail);
            return;
        }
    }

    private void GenerateMedia()
    {
        gameObjectECG_Arrow.SetActive(node.HasECG1ImageSample || node.HasECG2ImageSample);
        gameObjectXRay_Arrow.SetActive(node.HasXrayImageSample);
        gameObjectStethoscope_Arrow.SetActive(node.HasHeartAudioSample || node.HasLungAudioSample);

        if (node.HasECG1ImageSample)
        {
            gameObjectElectroencephalographicMachine.GetComponent<MeshRenderer>().materials[4].SetTexture("_MainTex", textureECG1);
        }
        if (node.HasECG2ImageSample)
            gameObjectElectroencephalographicMachine.GetComponent<MeshRenderer>().materials[4].SetTexture("_MainTex", textureECG2);

        if (node.HasXrayImageSample)
            gameObjectXRayView.GetComponent<MeshRenderer>().materials[2].SetTexture("_MainTex", textureXRay);

        if(!node.HasLungAudioSample)
        {
            gameObjectPatient_Arrow.SetActive(false);
        }
    }

    public void InspectLinkDoorBell()
    {
        //Activate Objects
        gameObjectPatient.SetActive(true);
        gameObjectXRay1.SetActive(true);

        //Deactivate Objects
        gameObjectDoorbell_Arrow.SetActive(false);

        //Create "Correct answer" soundeffect
        audioSourceXrayView.PlayOneShot(audioCorrectAnswer);

        //Title
        gameObjectCaseTitleQuestion.GetComponent<TextMesh>().text = node.NodeTitle;

        //Description
        gameObjectCaseDescriptionQuestion.GetComponent<TextMesh>().text = Converters.SpliceText(node.NodeText, Constants.SliceText_NodeLineLength);

        //Button message
        gameObjectAnswerMessage1.GetComponent<TextMesh>().text = node.NodeLinks[0].LinkText;
    }


    public void StethoscopePoint()
    {
        gameObjectStethoscope_Arrow.SetActive(false);
        gameObjectPatient_Arrow.SetActive(true);
        gameObjectStethoscopePoint.SetActive(true);
    }
}
