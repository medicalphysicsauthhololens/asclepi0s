﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
[CreateAssetMenu(fileName="CaseName",menuName= "VP/Case", order =1)]
public class Case : ScriptableObject {
	public int CaseUID; // ID needs to be maintained unique across all implementation.
	public string CaseTitle;
	public string CaseText;
    public List<Node> CaseNodes;
	public Node StartingNode;
	public Node EndNode;
}
