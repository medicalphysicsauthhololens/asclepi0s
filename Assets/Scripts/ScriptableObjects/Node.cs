﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//[System.Serializable]
[CreateAssetMenu(fileName="NodeName",menuName= "VP/Node", order =2)]
public class Node : ScriptableObject {
	public int NodeUID; // ID needs to be maintained unique across all implementation.
	public string NodeTitle;
	public string NodeText;
    public List<Link> NodeLinks;
    public bool HasHeartAudioSample;
    public bool HasLungAudioSample;
    public bool HasECG1ImageSample;
    public bool HasECG2ImageSample;
    public bool HasXrayImageSample;
}