﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActivateNode : MonoBehaviour {
	public ActivateLink LinkActivator;
	public List<Link> ActiveNodeLinks = new List<Link>();
	public void MakeNodeActive(Node ActiveNode){
		ActiveNodeLinks = ActiveNode.NodeLinks;	//populate NodeLink with the links from ActiveNode
	// call popup method for displaying in front of the user the text as GUI. 
		foreach (Link item in ActiveNodeLinks){// for each link in NodeLink call active link
			LinkActivator.MakeLinkActive(item);
			//substitute all this with game controller communication for transferring the node there. 
		}
	}
}
