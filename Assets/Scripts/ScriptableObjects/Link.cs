﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
[CreateAssetMenu(fileName="LinkName",menuName= "VP/Link", order =3)]
public class Link : ScriptableObject {
	public int LinkUID; // ID needs to be maintained unique across all implementation.
	public string LinkText;
	public string LinkResult;
    public Node TargetNode;
	public Button ActivationButton;
	public bool IsActive;
}
