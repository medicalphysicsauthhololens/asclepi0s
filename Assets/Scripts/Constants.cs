﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants {
    #region
    public static string Radiology_Diagnostic_Monitor_Message = "This is radiology diagnostic monitor";
    public static string Electroencephalographic_Machine_Message = "This is electroencephalographic machine";
    public static string Small_Monitor_Message = "This is small monitor";
    public static string Defibrillator_Emergency_Medicine_Message = "This is defibrillator emergency";
    public static string Blue_Stethoscope_Message = "This is blue stethoscope";
    #endregion

    public static int TILE_1 = 1;
    public static int TILE_2 = 2;
    public static int TILE_3 = 3;
    public static int TILE_4 = 4;
    public static int TILE_5 = 5;

    public static int SliceText_NodeLineLength = 65;
    public static int SliceText_LinkLineLength = 25;

}
