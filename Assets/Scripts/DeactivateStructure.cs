﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateStructure : MonoBehaviour {
    
	void Start () {
        GameObject gameObjectStethoscopePoint = GameObject.Find("StethoscopePoint");
        gameObjectStethoscopePoint.SetActive(false);

        GameObject gameObjectXRayTile5 = GameObject.Find("X-Ray_Tile_5");
        gameObjectXRayTile5.SetActive(false);

        GameObject gameObjectXRayTile3 = GameObject.Find("X-Ray_Tile_3");
        gameObjectXRayTile3.SetActive(false);

        GameObject gameObjectXRayTile2 = GameObject.Find("X-Ray_Tile_2");
        gameObjectXRayTile2.SetActive(false);

        GameObject gameObjectXRayTile4 = GameObject.Find("X-Ray_Tile_4");
        gameObjectXRayTile4.SetActive(false);

        GameObject gameObjectECG_Arrow = GameObject.Find("ECG_Arrow");
        gameObjectECG_Arrow.SetActive(false);

        GameObject gameObjectDoorbell_Arrow = GameObject.Find("Doorbell_Arrow");
        gameObjectDoorbell_Arrow.SetActive(false);

        GameObject gameObjectXRay_Arrow = GameObject.Find("XRay_Arrow");
        gameObjectXRay_Arrow.SetActive(false);

        GameObject gameObjectStethoscope_Arrow = GameObject.Find("Stethoscope_Arrow");
        gameObjectStethoscope_Arrow.SetActive(false);

        GameObject gameObjectPatient_Arrow = GameObject.Find("Patient_Arrow");
        gameObjectPatient_Arrow.SetActive(false);

        GameObject gameObjectPatient = GameObject.Find("Hairy_Man");
        gameObjectPatient.SetActive(false);
    }
}
