﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorbellOnSelect : MonoBehaviour
{
    void OnSelect()
    {
        //Push selection
        ScriptableObjectsHandler.Instance.InspectLinkDoorBell();
    }
}