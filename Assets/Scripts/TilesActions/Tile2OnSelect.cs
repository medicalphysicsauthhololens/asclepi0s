﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile2OnSelect : MonoBehaviour {

    void OnSelect()
    {
        //Push selection
        ScriptableObjectsHandler.Instance.InspectLink(Constants.TILE_2);
    }
}
