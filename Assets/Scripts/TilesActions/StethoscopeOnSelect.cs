﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StethoscopeOnSelect : MonoBehaviour {

    void OnSelect()
    {
        ScriptableObjectsHandler.Instance.StethoscopePoint();
    }
}
